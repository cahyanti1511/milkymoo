const dataMilkshake = [
	{
		nama: 'Milkshake Strawberry',
		harga: 'Rp 25.000',
		gambar: 'foto/ms.webp'
	}, {
		nama: 'Milkshake Bluberry',
		harga: 'Rp 30.000',
		gambar: 'foto/blm.jpg'
	}, {
		nama: 'Milkshake Chocolate',
		harga: 'Rp 25.000',
		gambar: 'foto/cm.jpg'
	}, {
		nama: 'Milkshake Oreo',
		harga: 'Rp 25.000',
		gambar: 'foto/mo.jpg'
	},
];


const dataSmoothies = [
	{
		nama: 'Smoothies Strawberry-Pisang',
		harga: 'Rp 45.000',
		gambar: 'foto/sp.jpg'
	}, {
		nama: 'Smoothies Kiwi-Mangga',
		harga: 'Rp 45.000',
		gambar: 'foto/km.jpg'
	}, {
		nama: 'Smoothies Blueberry-Strawberry',
		harga: 'Rp 55.000',
		gambar: 'foto/sb.jpg'
	}, {
		nama: 'Smoothies Blueberry-Pisang',
		harga: 'Rp 50.000',
		gambar: 'foto/bp.jpg'
	},
];

const dataFrappe = [
	{
		nama: 'Frappucino Oreo',
		harga: 'Rp 35.000',
		gambar: 'foto/oreo.jpg'
	}, {
		nama: 'Frappucino chocolate',
		harga: 'Rp 35.000',
		gambar: 'foto/fc.jpg'
	}, {
		nama: 'Frappucino matcha',
		harga: 'Rp 50.000',
		gambar: 'foto/mf.jpg'
	}, {
		nama: 'Frappucino Strawberry',
		harga: 'Rp 50.000',
		gambar: 'foto/fs.jpg'
	},
];

//map
const loopMenu = (item, index, array) =>{
	const namaItem		= item.nama.toLowerCase();
	let namaContainer;

	if (namaItem.includes('milkshake')) 
		namaContainer = document.querySelector('.container-milkshake');

	else if(namaItem.includes('smoothies'))
		namaContainer = document.querySelector('.container-smoothies');

	else if (namaItem.includes('frapp'))
		namaContainer = document.querySelector('.container-frappe');

		namaContainer.innerHTML += `
	<div class="card card-promo">
             <div class="animate-img">
                 <img src="${item.gambar}" class="card-img-top img-fluid" alt="card1">
             </div>
             <div class="card-body">
                 <h5 class="card-title">${item.nama}</h5>
                 <p class="card-text">${item.harga}</p>
                <a href="#" class="btn btn-outline-dark">Pesan</a>
            </div>
        </div>
	`
}
dataMilkshake.map(loopMenu);
dataSmoothies.map(loopMenu);
dataFrappe.map(loopMenu);

//filter

const inputKeyword = document.querySelector('.input-keyword')
.addEventListener('input', (event) => {
	const keyword = event.target.value.toLowerCase();
	const containerMilkshake = document.querySelector('.container-milkshake')
	const cariMenu = (item, index)=>{
		return item.nama.toLowerCase().includes(keyword);
	}

	const hasilMilkshakes = dataMilkshake.filter(cariMenu);
	const hasilSmoothies  = dataSmoothies.filter(cariMenu);
	const hasilFrappes	  = dataFrappe.filter(cariMenu);

	document.querySelector('.container-milkshake').innerHTML = '';
	document.querySelector('.container-smoothies').innerHTML = '';
	document.querySelector('.container-frappe').innerHTML = '';

	hasilMilkshakes.map(loopMenu);
	hasilSmoothies.map(loopMenu);
	hasilFrappes.map(loopMenu);


});

